package com.tsetserskaya.controller;


import com.tsetserskaya.Result;
import com.tsetserskaya.data.PetData;
import com.tsetserskaya.model.Pet;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
public class PetController {

    private static PetData data = new PetData();

    @RequestMapping(value = "/pet", method = RequestMethod.GET, produces={"application/json; charset=UTF-8"})
    public List getAllPets() {
        System.out.println("Received request to show all pets");
        return data.getAll();
    }

    @RequestMapping(value = "/pet/{petId}", method = RequestMethod.GET, produces={"application/json; charset=UTF-8"})
    public Pet getPetById(@PathVariable long petId) {
        System.out.println("Get pet by id");
        return data.getPetById(petId);
    }

    @RequestMapping(value = "/pet", method = RequestMethod.POST, produces={"application/json; charset=UTF-8"})
    public Result addNewPet(@RequestBody Pet pet) {
        System.out.println("Add pet");
        return data.addPet(pet);}

    @RequestMapping(value = "/pet", method = RequestMethod.PUT, produces={"application/json; charset=UTF-8"})
    public Result updatePet(@RequestBody Pet pet) {
        System.out.println("Update pet");
        return data.updatePet(pet);}

    @RequestMapping(value = "/pet/findByStatus", method = RequestMethod.GET, produces={"application/json; charset=UTF-8"})
    public List getAllPetsByStatus(@RequestParam(value = "status")String status) {
        System.out.println("Get all pets by status");
        return data.findPetByStatus(status);}

    @RequestMapping(value = "/pet/{petId}", method = RequestMethod.POST, produces={"application/json; charset=UTF-8"})
    public Result updatePetByFormData(@PathVariable long petId, @RequestParam("name")String name, @RequestParam("status")String status) {
        System.out.println("Update name and status");
        return data.updateByFormData(petId, name, status);
    }

    @RequestMapping(value = "/pet/{petId}", method = RequestMethod.DELETE, produces={"application/json; charset=UTF-8"})
    public Result deletePetById(@PathVariable long petId) {
        System.out.println("Delete pet");
        return data.deletePet(petId);
    }

    @RequestMapping(value = "/pet/{petId}/uploadImage", method = RequestMethod.POST, produces={"application/json; charset=UTF-8"})
    public Result updatePetByFormData(@PathVariable long petId, @RequestParam(value = "photoURL")String url) {
        System.out.println("Update url");
        return data.updateUrl(petId, url);
    }

}
