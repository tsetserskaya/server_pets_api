package com.tsetserskaya.data;


import com.tsetserskaya.Result;
import com.tsetserskaya.model.Category;
import com.tsetserskaya.model.Pet;

import java.util.ArrayList;
import java.util.List;

public class PetData {
    static List<Pet> pets = new ArrayList<>();
    static List<Category> categories = new ArrayList<>();

    static {
        categories.add(createCategory(1, "Dogs"));
        categories.add(createCategory(2, "Cats"));
        categories.add(createCategory(3, "Rabbits"));
        categories.add(createCategory(4, "Lions"));

        pets.add(createPet(1, categories.get(1), "Cat 1",
                "https://www.petfinder.com/wp-content/uploads/2012/11/91615172-find-a-lump-on-cats-skin-632x475.jpg", "available"));
        pets.add(createPet(2, categories.get(1), "Cat 2",
                "https://www.petfinder.com/wp-content/uploads/2012/11/152964589-welcome-home-new-cat-632x475.jpg",  "available"));
        pets.add(createPet(3, categories.get(1), "Cat 3",
                "url1", "pending"));

        pets.add(createPet(4, categories.get(0), "Dog 1",
                "https://www.what-dog.net/Images/faces2/scroll0015.jpg",  "available"));
        pets.add(createPet(5, categories.get(0), "Dog 2",
                "https://i.pinimg.com/736x/bb/16/5c/bb165c8fcecf107962691450d7505dd3--world-cutest-dog-cutest-dogs.jpg",  "sold"));
        pets.add(createPet(6, categories.get(0), "Dog 3",
                "https://www.cesarsway.com/sites/newcesarsway/files/styles/large_article_preview/public/Healthy-dog-treats-What-to-look-for-what-to-avoid.jpg?itok=P3GA1V9V",  "pending"));

        pets.add(createPet(7, categories.get(3), "Lion 1",
                "http://static-13.sinclairstoryline.com/resources/media/39fd7af6-b298-4113-9a9a-1e6cc9d189a2-large16x9_Bakari_KarenCasterphoto.jpg",  "available"));
        pets.add(createPet(8, categories.get(3), "Lion 2",
                "url1",  "available"));
        pets.add(createPet(9, categories.get(3), "Lion 3",
                "https://i.pinimg.com/736x/da/af/73/daaf73960eb5a21d6bca748195f12052--lion-photography-lion-kings.jpg",  "available"));

        pets.add(createPet(10, categories.get(2), "Rabbit 1",
                "https://hobbydb-production.s3.amazonaws.com/processed_uploads/subject_photo/subject_photo/image/4245/1422416006-3-3646/rabbit.jpg",  "available"));
    }

    public Pet getPetById(long petId) {
        for (Pet pet : pets) {
            if (pet.getId() == petId) {
                return pet;
            }
        }
        return null;
    }

    public List<Pet> findPetByStatus(String status) {
        String[] statues = status.split(",");
        List<Pet> result = new ArrayList<>();
        for (Pet pet : pets) {
            for (String s : statues) {
                if (s.equals(pet.getStatus())) {
                    result.add(pet);
                }
            }
        }
        return result;
    }


    public Result updateByFormData(long petId, String name, String status) {
        Result res = new Result("",0);
        for (Pet pet : pets) {
            if (pet.getId() == petId) {
                pet.setName(name);
                pet.setStatus(status);
                res.setResult("Find and Update");
                res.setValue(1);
            }
        }

        return res;

    }

    public Result updateUrl(long petId, String url) {
        Result res = new Result("",0);
        for (Pet pet : pets) {
            if (pet.getId() == petId) {
                pet.setPhotoUrl(url);
                res.setResult("Find and Set");
                res.setValue(1);
            }
        }

        return res;
    }


    public List<Pet> getAll() {
        return pets;
    }



    public Result addPet(Pet pet) {
        Result res = new Result();
        if (pets.size() > 0) {
            for (int i = pets.size() - 1; i >= 0; i--) {
                if (pets.get(i).getId() == pet.getId()) {
                    pets.remove(i);
                    res.setResult("Find and Add");
                    res.setValue(i);
                }
            }
        }
        pets.add(pet);
        return res;
    }

    public Result deletePet(long id) {

        Result res = new Result();
        if (pets.size() > 0) {
            for (int i = pets.size() - 1; i >= 0; i--) {
                if (pets.get(i).getId() == id) {
                    pets.remove(i);
                    res.setResult("Find and Delete");
                    res.setValue(i);
                }
                else { res.setValue(0); res.setResult("No data");}
            }
        }

        return res;
    }

    public Result updatePet(Pet pet) {
        Result res = new Result();
        if (pets.size() > 0) {
            for (int i = pets.size() - 1; i >= 0; i--) {
                if (pets.get(i).getId() == pet.getId()) {
                    pets.remove(i);
                    res.setResult("Find and Update");
                    res.setValue(i);
                }
            }
        }
        pets.add(pet);
        return res;
    }

    static Pet createPet(long id, Category cat, String name, String url, String status) {
        Pet pet = new Pet();
        pet.setId(id);
        pet.setCategory(cat);
        pet.setName(name);
        if (null != url) {
            pet.setPhotoUrl(url);
        }
        pet.setStatus(status);
        return pet;
    }

    static Category createCategory(long id, String name) {
        Category category = new Category();
        category.setId(id);
        category.setName(name);
        return category;
    }
}